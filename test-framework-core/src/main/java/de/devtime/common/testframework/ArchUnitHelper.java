package de.devtime.common.testframework;

import static org.junit.jupiter.api.Assertions.fail;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.springframework.util.ReflectionUtils;

import com.tngtech.archunit.base.DescribedPredicate;
import com.tngtech.archunit.core.domain.JavaClass;
import com.tngtech.archunit.core.domain.JavaField;
import com.tngtech.archunit.core.domain.JavaMethod;
import com.tngtech.archunit.core.domain.JavaType;
import com.tngtech.archunit.core.importer.ClassFileImporter;

@SuppressWarnings("javadoc")
public class ArchUnitHelper {

  private static final String BUILDER_METHOD_NAME = "builder";
  private static final String FILL_WITH_RANDOM_DATA_METHOD_NAME = "fillWithRandomData";
  private static final String BUILD_METHOD_NAME = "build";

  public static Object getBuilderOf(JavaClass javaClass) {
    Object builder = null;
    try {
      JavaMethod javaMethod = javaClass.getMethod(BUILDER_METHOD_NAME);
      builder = javaMethod.reflect().invoke(javaClass.reflect());
    } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
      fail("Can't invoke builder method from class " + javaClass.getName(), e);
    }
    return builder;
  }

  public static Object getFieldValue(JavaClass javaClass, String fieldName, Object obj) {
    Object fieldValue = null;
    try {
      fieldValue = FieldUtils.getDeclaredField(javaClass.reflect(), fieldName, true).get(obj);
    } catch (IllegalAccessException e) {
      fail("Can't access the field " + fieldName + " in class " + javaClass.getName());
    }
    return fieldValue;
  }

  public static List<JavaMethod> getAllMethodsWithPrefix(Object builder, String prefix, String... ignore) {
    List<String> ignoreList = Arrays.asList(ignore);
    return new ClassFileImporter()
        .importClass(builder.getClass())
        .getAllMethods().stream()
        .filter(javaMethod -> !ignoreList.contains(javaMethod.getName()))
        .filter(javaMethod -> javaMethod.getName().startsWith(prefix))
        .toList();
  }

  public static String getMethodOfField(JavaField javaField, String prefix) {
    return new StringBuilder()
        .append(prefix)
        .append(Character.toUpperCase(javaField.getName().charAt(0)))
        .append(javaField.getName().substring(1))
        .toString();
  }

  public static String getFieldNameOfMethod(JavaMethod javaMethod, String prefix) {
    return new StringBuilder()
        .append(Character.toLowerCase(javaMethod.getName().charAt(prefix.length())))
        .append(javaMethod.getName().substring(prefix.length() + 1))
        .toString();
  }

  public static String getGetterNameOfFieldName(String fieldName) {
    return new StringBuilder()
        .append("get")
        .append(Character.toUpperCase(fieldName.charAt(0)))
        .append(fieldName.substring(1))
        .toString();
  }

  public static String getSetterNameOfFieldName(String fieldName) {
    return new StringBuilder()
        .append("set")
        .append(Character.toUpperCase(fieldName.charAt(0)))
        .append(fieldName.substring(1))
        .toString();
  }

  public static JavaClass getSingleParameterType(JavaMethod javaMethod) {
    JavaClass type = null;
    List<JavaType> types = javaMethod.getParameterTypes();
    if (types.size() == 1) {
      type = types.get(0).toErasure();
    } else {
      fail("The method " + javaMethod.getName() + " does not has exactly one parameter.");
    }
    return type;
  }

  public static boolean hasBuilder(JavaClass javaClass) {
    return javaClass.tryGetMethod(BUILDER_METHOD_NAME).isPresent();
  }

  public static boolean hasDefaultConstructor(JavaClass javaClass) {
    return javaClass.tryGetConstructor().isPresent();
  }

  public static boolean hasFillWithRandomData(JavaClass javaClass) {
    return javaClass.getMethod(FILL_WITH_RANDOM_DATA_METHOD_NAME) != null;
  }

  public static boolean hasSetter(JavaClass javaClass) {
    boolean result = false;
    for (JavaMethod method : javaClass.getAllMethods()) {
      if (method.getName().startsWith("set")) {
        result = true;
        break;
      }
    }
    return result;
  }

  public static boolean hasSetter(JavaClass javaClass, String setterMethodName) {
    boolean result = false;
    for (JavaMethod method : javaClass.getAllMethods()) {
      if (method.getName().equals(setterMethodName)) {
        result = true;
        break;
      }
    }
    return result;
  }

  public static boolean hasPublicGetter(JavaClass javaClass, String... ignoreMethodNames) {
    List<String> ignoreList = Arrays.asList(ignoreMethodNames);
    boolean result = false;
    for (JavaMethod method : javaClass.getAllMethods()) {
      boolean ignore = ignoreList.contains(method.getName());
      boolean isPublic = Modifier.isPublic(method.reflect().getModifiers());
      boolean startsWithGet = method.getName().startsWith("get");
      if (!ignore && isPublic && startsWithGet) {
        result = true;
        break;
      }
    }
    return result;
  }

  public static boolean hasGetter(JavaClass javaClass, String getterMethodName) {
    boolean result = false;
    for (JavaMethod method : javaClass.getAllMethods()) {
      System.out.println("method: " + method.getName() + ", getterMethodName: " + getterMethodName);
      if (method.getName().equals(getterMethodName)) {
        result = true;
        break;
      }
    }
    return result;
  }

  public static Object invokeDefaultConstructor(JavaClass javaClass) {
    Object newInstance = null;
    try {
      Constructor<?> defaultConstructor = javaClass.getConstructor().reflect();
      defaultConstructor.setAccessible(true);
      newInstance = defaultConstructor.newInstance();
    } catch (Exception e) {
      fail("Can't create a new instance with the default constructor of class " + javaClass.getName(), e);
    }
    return newInstance;
  }

  public static Object invokeBuild(Object target) {
    Object result = null;
    Method method = ReflectionUtils.findMethod(target.getClass(), BUILD_METHOD_NAME);
    if (method == null) {
      fail("No build method found in builder of type " + target.getClass());
    } else {
      result = ReflectionUtils.invokeMethod(method, target);
    }
    return result;
  }

  public static Object invokeGetter(Object target, String getterMethodName) {
    Object result = null;
    try {
      result = MethodUtils.invokeExactMethod(target, getterMethodName);
    } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
      fail("Can't invoke getter " + getterMethodName + " from class " + target.getClass(), e);
    }
    return result;
  }

  public static void invokeSetter(Object target, String setterMethodName, Object value) {
    try {
      MethodUtils.invokeExactMethod(target, setterMethodName, value);
    } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
      fail("Can't invoke setter " + setterMethodName + " from class " + target.getClass(), e);
    }
  }

  public static Map<String, Object> invokeAllMethodsAndSetRandomParameter(Object obj, String prefix, String... ignore) {
    Map<String, Object> valueMap = new HashMap<>();
    List<String> methodNameToIgnore = Arrays.asList(ignore);
    getAllMethodsWithPrefix(obj, prefix).stream()
        .filter(javaMethod -> !methodNameToIgnore.contains(javaMethod.getName()))
        .forEach(javaMethod -> {
          Object value = generateRandomValueForType(getSingleParameterType(javaMethod));
          ReflectionUtils.invokeMethod(javaMethod.reflect(), obj, value);
          valueMap.put(javaMethod.getName(), value);
        });
    return valueMap;
  }

  public static Object generateRandomValueForType(JavaClass parameterType) {
    Object value = null;
    Class<?> clazz = parameterType.reflect();
    if (clazz == String.class) {
      value = RandomStringUtils.randomAlphabetic(10);
    } else if (clazz == int.class || clazz == Integer.class) {
      value = RandomUtils.nextInt(0, 100);
    } else if (Enum.class.isAssignableFrom(clazz)) {
      Method method = ReflectionUtils.findMethod(clazz, "values");
      Object[] values = (Object[]) ReflectionUtils.invokeMethod(method, parameterType);
      value = values[RandomUtils.nextInt(0, values.length)];
    } else if (hasBuilder(parameterType)) {
      Object builder = getBuilderOf(parameterType);
      Method method = ReflectionUtils.findMethod(builder.getClass(), FILL_WITH_RANDOM_DATA_METHOD_NAME);
      if (method == null) {
        fail("No fillWithRandomData method found in builder of type " + parameterType);
      } else {
        ReflectionUtils.invokeMethod(method, builder);
        value = invokeBuild(builder);
      }
    } else {
      fail("No rule to generate random value for type " + parameterType);
    }
    return value;
  }

  public static void writeField(Object target, String fieldName, Object value) {
    try {
      FieldUtils.writeField(target, fieldName, value, true);
    } catch (IllegalAccessException e) {
      fail("Can't write the value <" + value + "> to field <" + fieldName + "> of class <" + target.getClass() + ">.",
          e);
    }
  }

  public static DescribedPredicate<JavaClass> arePersistenceObjects() {
    return new DescribedPredicate<JavaClass>("only persistence object classes") {

      @Override
      public boolean apply(JavaClass javaClass) {
        return javaClass.isAssignableTo("de.devtime.common.utilities.interfaces.PersistenceObject");
      }
    };
  }

  public static DescribedPredicate<JavaClass> areBusinessObjects() {
    return new DescribedPredicate<JavaClass>("only business object classes") {

      @Override
      public boolean apply(JavaClass javaClass) {
        return javaClass.isAssignableTo("de.devtime.common.utilities.interfaces.BusinessObject");
      }
    };
  }

  public static DescribedPredicate<JavaClass> areAnnotatedWithEntity() {
    return new DescribedPredicate<JavaClass>("only classes with @Entity annotation") {

      @Override
      public boolean apply(JavaClass javaClass) {
        return javaClass.isAnnotatedWith("javax.persistence.Entity");
      }
    };
  }

  public static DescribedPredicate<JavaClass> areClassesWithBuilder() {
    return new DescribedPredicate<JavaClass>("only classes with a builder") {

      @Override
      public boolean apply(JavaClass javaClass) {
        return hasBuilder(javaClass);
      }
    };
  }

  public static DescribedPredicate<JavaClass> areClassesWithSetter() {
    return new DescribedPredicate<JavaClass>("only classes with setter methods") {

      @Override
      public boolean apply(JavaClass javaClass) {
        return hasSetter(javaClass);
      }
    };
  }

  public static DescribedPredicate<JavaClass> areClassesWithPublicGetter(String... ignore) {
    return new DescribedPredicate<JavaClass>("only classes with getter methods") {

      @Override
      public boolean apply(JavaClass javaClass) {
        return hasPublicGetter(javaClass, ignore);
      }
    };
  }

  private ArchUnitHelper() {
    // private utility class constructor
  }
}
