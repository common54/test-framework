package de.devtime.common.testframework;

import static de.devtime.common.testframework.ArchUnitHelper.areBusinessObjects;
import static de.devtime.common.testframework.ArchUnitHelper.areClassesWithBuilder;
import static de.devtime.common.testframework.ArchUnitHelper.areClassesWithPublicGetter;
import static de.devtime.common.testframework.ArchUnitHelper.areClassesWithSetter;
import static de.devtime.common.testframework.ArchUnitHelper.arePersistenceObjects;
import static de.devtime.common.testframework.ArchUnitHelper.generateRandomValueForType;
import static de.devtime.common.testframework.ArchUnitHelper.getAllMethodsWithPrefix;
import static de.devtime.common.testframework.ArchUnitHelper.getBuilderOf;
import static de.devtime.common.testframework.ArchUnitHelper.getFieldNameOfMethod;
import static de.devtime.common.testframework.ArchUnitHelper.getFieldValue;
import static de.devtime.common.testframework.ArchUnitHelper.getGetterNameOfFieldName;
import static de.devtime.common.testframework.ArchUnitHelper.getMethodOfField;
import static de.devtime.common.testframework.ArchUnitHelper.getSingleParameterType;
import static de.devtime.common.testframework.ArchUnitHelper.hasBuilder;
import static de.devtime.common.testframework.ArchUnitHelper.hasDefaultConstructor;
import static de.devtime.common.testframework.ArchUnitHelper.hasGetter;
import static de.devtime.common.testframework.ArchUnitHelper.hasSetter;
import static de.devtime.common.testframework.ArchUnitHelper.invokeAllMethodsAndSetRandomParameter;
import static de.devtime.common.testframework.ArchUnitHelper.invokeBuild;
import static de.devtime.common.testframework.ArchUnitHelper.invokeDefaultConstructor;
import static de.devtime.common.testframework.ArchUnitHelper.invokeGetter;
import static de.devtime.common.testframework.ArchUnitHelper.invokeSetter;
import static de.devtime.common.testframework.ArchUnitHelper.writeField;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.util.ReflectionUtils;

import com.tngtech.archunit.core.domain.JavaClass;
import com.tngtech.archunit.core.domain.JavaField;
import com.tngtech.archunit.core.importer.ClassFileImporter;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuppressWarnings("javadoc")
public class LombokTestHelper {

  public static boolean allPersistenceObjectClassesShouldProvideABuilder(String packageName) {
    new ClassFileImporter()
        .importPackages(packageName)
        .that(arePersistenceObjects())
        .forEach(javaClass -> {
          log.info("Test builder existence in class: {}", javaClass.getName());

          // Every persistenceobject class should have a builder that can be received via static builder() method
          assertThat(hasBuilder(javaClass), is(equalTo(true)));
        });
    return true;
  }

  public static boolean allBusinessObjectClassesShouldProvideABuilder(String packageName) {
    new ClassFileImporter()
        .importPackages(packageName)
        .that(areBusinessObjects())
        .forEach(javaClass -> {
          log.info("Test builder existence in class: {}", javaClass.getName());

          // Every businessobject class should have a builder that can be received via static builder() method
          assertThat(hasBuilder(javaClass), is(equalTo(true)));
        });
    return true;
  }

  public static boolean testAllBuilder(String packageName, boolean useGetter, String... onlyNotNullFieldNames) {
    List<String> notNullFieldNames = Arrays.asList(onlyNotNullFieldNames);
    new ClassFileImporter()
        .importPackages(packageName)
        .that(areClassesWithBuilder())
        .forEach(javaClass -> {
          log.info("Test builder in class: {}", javaClass.getName());

          // The builder should be able to be created successfully via the builder() method
          Object builder = getBuilderOf(javaClass);
          assertThat(builder, is(notNullValue()));

          // All fields must be able to be set with random values via the builder
          JavaClass builderJavaClass = new ClassFileImporter().importClass(builder.getClass());
          Map<String, Object> expectedValueMap = invokeAllMethodsAndSetRandomParameter(builder, "with");
          builderJavaClass.getAllFields().stream()
              .filter(javaField -> expectedValueMap.containsKey(getMethodOfField(javaField, "with")))
              .forEach(javaField -> {
                log.debug("verify field from builder: {}", javaField.getName());
                Object fieldValue = getFieldValue(builderJavaClass, javaField.getName(), builder);
                Object expectedValue = expectedValueMap.get(getMethodOfField(javaField, "with"));
                assertThat(getFailureMessage(builderJavaClass, javaField), fieldValue, is(equalTo(expectedValue)));
              });

          // Calling build must create a new instance of the class to build
          Object buildedObject = invokeBuild(builder);
          assertThat(buildedObject, is(notNullValue()));

          // All fields of the builded objects must match the expected values
          javaClass.getAllFields().stream()
              .filter(javaField -> expectedValueMap.containsKey(getMethodOfField(javaField, "with")))
              .forEach(javaField -> {
                log.debug("verify field from builded object: {}", javaField.getName());

                Object fieldValue = null;
                if (useGetter && hasGetter(javaClass, getGetterNameOfFieldName(javaField.getName()))) {
                  fieldValue = invokeGetter(buildedObject, getGetterNameOfFieldName(javaField.getName()));
                } else {
                  fieldValue = getFieldValue(javaClass, javaField.getName(), buildedObject);
                }

                if (notNullFieldNames.contains(javaField.getName())) {
                  assertThat(getFailureMessage(javaClass, javaField), fieldValue, is(notNullValue()));
                } else {
                  Object expectedValue = expectedValueMap.get(getMethodOfField(javaField, "with"));
                  assertThat(getFailureMessage(javaClass, javaField), fieldValue, is(equalTo(expectedValue)));
                }
              });
        });
    return true;
  }

  public static boolean testAllSetter(String packageName, boolean useGetter) {
    new ClassFileImporter()
        .importPackages(packageName)
        .that(areClassesWithSetter())
        .forEach(javaClass -> {
          log.info("Test setter of class: {}", javaClass.getName());

          final Object obj;
          if (hasDefaultConstructor(javaClass)) {
            obj = invokeDefaultConstructor(javaClass);
          } else if (hasBuilder(javaClass)) {
            obj = invokeBuild(getBuilderOf(javaClass));
          } else {
            log.info("No default constructor and no builder is available. Test was skipped.");
            obj = null;
          }

          if (obj != null) {
            getAllMethodsWithPrefix(obj, "set").stream()
                .forEach(javaMethod -> {
                  log.debug("invoke method {}", javaMethod.getFullName());

                  // Invoke all setter with random data
                  Object value = generateRandomValueForType(getSingleParameterType(javaMethod));
                  ReflectionUtils.invokeMethod(javaMethod.reflect(), obj, value);

                  // Verfiy field value behind the setter
                  String fieldName = getFieldNameOfMethod(javaMethod, "set");
                  Object actualValue;
                  if (useGetter) {
                    actualValue = invokeGetter(obj, getGetterNameOfFieldName(fieldName));
                  } else {
                    actualValue = getFieldValue(javaClass, fieldName, obj);
                  }
                  assertThat(actualValue, is(equalTo(value)));
                });
          }

        });
    return true;
  }

  public static boolean testAllGetter(String packageName, boolean useSetter) {
    String[] ignore = {
        "getClass"
    };
    new ClassFileImporter()
        .importPackages(packageName)
        .that(areClassesWithPublicGetter(ignore))
        .forEach(javaClass -> {
          log.info("Test getter of class: {}", javaClass.getName());

          final Object obj;
          if (hasDefaultConstructor(javaClass)) {
            obj = invokeDefaultConstructor(javaClass);
          } else if (hasBuilder(javaClass)) {
            obj = invokeBuild(getBuilderOf(javaClass));
          } else {
            log.info("No default constructor and no builder is available. Test was skipped.");
            obj = null;
          }

          if (obj != null) {
            getAllMethodsWithPrefix(obj, "get", ignore).stream()
                .forEach(javaMethod -> {
                  log.debug("invoke method {}", javaMethod.getFullName());

                  // Set random data into field
                  Object value = generateRandomValueForType(javaMethod.getReturnType().toErasure());
                  String fieldName = getFieldNameOfMethod(javaMethod, "get");
                  boolean skipp = false;
                  if (useSetter) {
                    String setterMethodName = new StringBuilder()
                        .append("set")
                        .append(Character.toUpperCase(fieldName.charAt(0)))
                        .append(fieldName.substring(1))
                        .toString();
                    if (hasSetter(javaClass, setterMethodName)) {
                      invokeSetter(obj, setterMethodName, value);
                    } else {
                      log.warn("No setter found for field <" + fieldName + ">. Test was skipped.");
                      skipp = true;
                    }
                  } else {
                    writeField(obj, fieldName, value);
                  }

                  if (!skipp) {
                    // Invoke all getter and verify value
                    Object actualValue = ReflectionUtils.invokeMethod(javaMethod.reflect(), obj);
                    assertThat(actualValue, is(equalTo(value)));
                  }
                });
          } else {
            log.info("No default constructor is available. Test was skipped.");
          }
        });
    return true;
  }

  private static String getFailureMessage(JavaClass javaClass, JavaField javaField) {
    return new StringBuilder()
        .append("The field <").append(javaField.getName()).append("> ")
        .append("in class <").append(javaClass.getName()).append("> ")
        .append("does not match with the expected value.")
        .toString();
  }

  private LombokTestHelper() {
    // private utility class constructor
  }
}
